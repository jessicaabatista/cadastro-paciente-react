import Paciente from './Paciente';
import "../index.css"

const Pacientes = ({ pacientes, onDelete, onEdit }) => {
    return (
        <>
            {pacientes.map((paciente) => (<Paciente key={paciente.id} paciente={paciente} onDelete={onDelete} onEdit={onEdit} />))}
        </>
    )
}

export default Pacientes;
