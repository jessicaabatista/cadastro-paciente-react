import { useState } from 'react';
import Swal from "sweetalert2";

const AddPaciente = ({ onSave }) => {
    const [nome, setNome] = useState('');
    const [day, setDay] = useState('');
    const [cpf, setCPF] = useState('');
    const [sexo, setSexo] = useState('');
    const [endereco, setEndereco] = useState('');
    const [status, setStatus] = useState('');

    const onSubmit = (e) => {
        e.preventDefault();

        if (!nome || !day || !cpf || !sexo || !status) {
            Swal.fire({
                icon: 'error',
                title: 'Erro!',
                text: 'É necessário preencher: Nome; Data de nasc.; CPF; Sexo; Status.'
            })
        } else {   

            let data = JSON.parse(localStorage.getItem('pacienteAdded'));

            data.map(x => {
                if (x.cpf === cpf) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro!',
                        text: 'Esse CPF já foi cadastrado.'
                    })
                } else {
                    onSave({ nome, day, cpf, sexo, endereco, status });   
                }
            })

            
            // onSave({ nome, day, cpf, sexo, endereco, status });        
        }

        setNome('');
        setDay('');
        setCPF('');
        setSexo('');
        setEndereco('');
        setStatus('');
    }

    return (
        <form className="add-form" onSubmit={onSubmit}>
            <div className="form-control">
                <label>Nome</label>
                <input type="text" value={nome} onChange={(e) => setNome(e.target.value)} />
            </div>
            <div className="form-control">
                <label>Data Nascimento</label>
                <input type="date" value={day} onChange={(e) => setDay(e.target.value)} />
            </div>
            <div className="form-control">
                <label>CPF</label>
                <input type="text" name="cpf" id="cpf" maxlength="11" value={cpf} onChange={(e) => setCPF(e.target.value)} />
            </div>
            <div className="form-control">
                <label>Sexo</label>
                <select name="select" value={sexo} onChange={(e) => setSexo(e.target.value)}>
                    <option>Não Informado</option>
                    <option value="Feminino">Feminino</option>
                    <option value="Masculino">Masculino</option>
                </select>
            </div>
            <div className="form-control">
                <label>Endereço</label>
                <input type="text" value={endereco} onChange={(e) => setEndereco(e.target.value)} />
            </div>
            <div className="form-control">
                <label>Status</label>
                <select name="select" value={status} onChange={(e) => setStatus(e.target.value)}>
                    <option>Não Informado</option>
                    <option value="Ativo">Ativo</option>
                    <option value="Dasativado">Dasativado</option>
                </select>
            </div>

            <input onClick="valida()" type="submit" className="btn btn-block" value="Save Paciente" />
        </form>
    )
}

export default AddPaciente
