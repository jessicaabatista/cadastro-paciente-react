import { FaPencilAlt, FaTimes } from 'react-icons/fa';
import "../index.css"

const Paciente = ({ paciente, onDelete, onEdit }) => {
    return (
        <div>
            <div className="paciente">
                <div>
                    <p className="pacienteName">
                        <span className="textBold">Nome:</span> {paciente.nome}
                    </p>
                    <p className="pacienteDate">
                        <span className="textBold">Data de nascimento:</span> {paciente.day}
                    </p>
                    <p className="pacienteCPF">
                        <span className="textBold">CPF:</span> {paciente.cpf}
                    </p>
                    <p className="pacienteSexo">
                        <span className="textBold">Sexo:</span> {paciente.sexo}
                    </p>
                    <p className="pacienteEndereco">
                        <span className="textBold">Endereço:</span> {paciente.endereco}
                    </p>
                    <p className="pacienteStatus">
                        <span className="textBold">Status:</span> {paciente.status}
                    </p>
                </div>
                <div>
                    <p><FaTimes onClick={() => onDelete(paciente.id)} className="delIcon" /></p>
                    <p><FaPencilAlt onClick={() => onEdit(paciente.id)} className="editIcon" /></p>
                </div>
            </div>
        </div>
    )
}

export default Paciente
