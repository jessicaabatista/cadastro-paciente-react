// Importing Components
import Header from './components/Header';
import Pacientes from './components/Pacientes';
import AddPaciente from './components/AddPaciente';
// Importing React Hooks
import { useState, useEffect } from 'react';
// Importing Packages
import { v4 as uuidv4 } from 'uuid';
import Swal from "sweetalert2";

function App() {
    // All States
    const [loading, setloading] = useState(true); // Pre-loader before page renders
    const [pacientes, setPacientes] = useState([]); // Paciente State
    const [showAddPaciente, setShowAddPaciente] = useState(false); // To reveal add paciente form

    // Pre-loader
    useEffect(() => {
        setTimeout(() => {
            setloading(false);
        }, 3500);
    }, [])

    // Fetching from Local Storage
    const getPacientes = JSON.parse(localStorage.getItem("pacienteAdded"));

    useEffect(() => {
        if (getPacientes == null) {
            setPacientes([])
        } else {
            setPacientes(getPacientes);
        }
        // eslint-disable-next-line
    }, [])

    // Add Paciente
    const addPaciente = (paciente) => {
        const id = uuidv4();
        const newPaciente = { id, ...paciente }

        setPacientes([...pacientes, newPaciente]);

        Swal.fire({
            icon: 'success',
            title: 'Sucesso!',
            text: 'O cadastro foi realizado.'
        })

        localStorage.setItem("pacienteAdded", JSON.stringify([...pacientes, newPaciente]));
    }

    // Delete Paciente
    const deletePaciente = (id) => {
        const deletePaciente = pacientes.filter((paciente) => paciente.id !== id);

        setPacientes(deletePaciente);

        Swal.fire({
            icon: 'success',
            title: 'Atenção!',
            text: 'O cadastro foi deletado.'
        })

        localStorage.setItem("pacienteAdded", JSON.stringify(deletePaciente));
    }

    // Edit Paciente
    const editPaciente = (id) => {

        const nome = prompt("Nome");
        const day = prompt("Data de nascimento");
        const cpf = prompt("CPF");
        const sexo = prompt("Sexo");
        const endereco = prompt("Endereço");
        const status = prompt("Status");

        let data = JSON.parse(localStorage.getItem('pacienteAdded'));

        const myData = data.map(x => {
            if (x.id === id) {
                return {
                    ...x,
                    nome: nome,
                    day: day,
                    cpf: cpf,
                    sexo: sexo,
                    endereco: endereco,
                    status: status,
                    id: uuidv4()
                }
            }
            return x;
        })

        Swal.fire({
            icon: 'success',
            title: 'Sucesso!',
            text: 'O cadastro foi editado.'
        })

        localStorage.setItem("pacienteAdded", JSON.stringify(myData));
        window.location.reload();
    }

    // Find Paciente
    const findPaciente = (id) => {

        const nome = prompt("Nome");

        let data = JSON.parse(localStorage.getItem('pacienteAdded'));

        const myData = data.map(x => {
            if (x.nome === nome) {
                return x;
            }
        })

        Swal.fire({
            icon: 'success',
            title: 'Sucesso!',
            text: 'O cadastro foi editado.'
        })

        localStorage.setItem("pacienteAdded", JSON.stringify(myData));
        window.location.reload();
    }

    return (
        <>
            {
                loading
                    ?
                    <div className="spinnerContainer">
                        <div className="spinner-grow text-primary" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                        <div className="spinner-grow text-secondary" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                        <div className="spinner-grow text-success" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                        <div className="spinner-grow text-danger" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                        <div className="spinner-grow text-warning" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                    :
                    <div className="container">
                        {/* App Header that has open and App Name */}
                        <Header showForm={() => setShowAddPaciente(!showAddPaciente)} changeTextAndColor={showAddPaciente} />

                        {/* Revealing of Add Paciente Form */}
                        {showAddPaciente && <AddPaciente onSave={addPaciente} />}

                        {/* Paciente Counter */}
                        <h3>Quantidade de cadastros: {pacientes.length}</h3>

                        {/* Displaying of Pacientes */}
                        {
                            pacientes.length > 0
                                ?
                                (<Pacientes pacientes={pacientes} onDelete={deletePaciente} onEdit={editPaciente} />)
                                :
                                ('Cadastro não encontrado!')
                        }
                    </div>
            }
        </>
    )
}

export default App;