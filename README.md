# cadastro-paciente-react

**CADASTRO DE PACIENTES**

A clínica ACME deseja ter uma solução web para consultar e
cadastrar seus pacientes.
O cadastro de um paciente deve possuir as seguintes
informações:
- Nome (obrigatório)
- Data nascimento (obrigatório)
- CPF (obrigatório)
- Sexo (obrigatório)
- Endereço (opcional)
- Status (obrigatório – Ativo/Inativo)

O sistema deve validar para não permitir CPF duplicado.

Para a persistência dos dados pode ser utilizado o localstorage.
Futuramente a clínica irá ter uma camada de serviço onde os
pacientes ficarão registrados em nuvem.

REQUISITOS:
- Listar pacientes, com opção de filtro pelo nome;
- Cadastrar/editar paciente;
- Inativar paciente;
